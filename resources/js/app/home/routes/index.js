import { GameThreeOnline } from "../components";

export default [
    {
        path: '/gamethreeonline',
        component: GameThreeOnline,
        name: 'gamethreeonline',
        meta: {
            guest: true,
        }
    }
]