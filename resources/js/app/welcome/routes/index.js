import { Welcome } from "../components";

export default [
    {
        path: '/',
        component: Welcome,
        name: 'welcome',
        meta: {
            guest: true,
        }
    }
]