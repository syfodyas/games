import home from './home/routes'
import welcome from './welcome/routes'

export default [...home, ...welcome]