<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function(){
    return view('home');
});

Route::get('/board', [
    'as'    => 'create_board',
    'uses'  => 'BoardController@load_board'
]);

/*Ajax*/
Route::post('new_game', [
    'as'    => 'new_game',
    'uses'   => 'GameController@newGame',
]);

Route::post('save_turn', [
    'as'    => 'save_turn',
    'uses'  => 'GameController@saveTurn'
]);
