<?php

namespace App\Http\Controllers;

use App\Turn;
use App\Game;
use App\Round;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function newGame(Request $request){
        $game_id = $request->post('game');
        $round = new Round();
        $round->game_id = $game_id;
        $round->save();

        $comb_win = DB::table('combinations_three_online')
                                ->select('position_one', 'position_two', 'position_three')
                                ->get();

        return response()->json(
            array(
                'success' => true,
                'last_insert_id' => $round->id,
                'combinations' => $comb_win,
            ), 200);
    }

    public function saveTurn(Request $request){
        $round = $request->post('round');
        $player = $request->post('player');
        $position = $request->post('position');

        $turn = new Turn();
        $turn->round_id = $round;
        $turn->position = $position;
        $turn->player = $player;
        $turn->save();

        return response()->json(
            array(
                'success' => true,
                'last_insert_id' => $turn->id
            ), 200);
    }
}
